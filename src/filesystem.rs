use std::path::Path;

pub fn is_existing_directory(path: &Path) -> bool {
	path.is_dir() && (path.file_name().is_some() || path.canonicalize().is_ok())
}