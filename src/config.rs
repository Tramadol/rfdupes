use crate::error::Error;

use parse_size::parse_size;

#[derive(Debug)]
pub struct SearchConfig {
	// Flags
	pub recursive: bool,
	pub ignore_empty_files: bool,
	pub ignore_hidden_files: bool,

	// Options
	pub minsize: u64,
	pub maxsize: u64
}

pub fn parse_config_size(size_option: Option<&str>) -> Result<Option<u64>, Error> {
	if size_option.is_none() {
		Ok(None)
	} else {
		let option_result = match parse_size(size_option.unwrap()) {
			Ok(value) => value,
			Err(_) => return Err(Error::new(format!(
				"{} is an invalid value for <size>.",
				size_option.unwrap()
			)))
		};
		Ok(Some(option_result))
	}
}