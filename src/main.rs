#![deny(unsafe_code)]

mod app;
mod filesystem;
mod error;
mod config;
mod scan;

use std::process::exit;
use std::env;
use std::path::{Path, PathBuf};

use crate::config::{SearchConfig, parse_config_size};
use crate::error::{Error, print_stderr};

fn main() {
	let result = run();

	match result {
		Err(error) => {
			print_stderr(error);
			exit(1);
		}
		Ok(false) => {
			exit(1);
		}
		Ok(true) => {
			exit(0);
		}
	}
}

fn run() -> Result<bool, Error> {
	let matches = app::build_application().get_matches_from(env::args_os());

	// Use current directory if no directory is specified
	let current_dir = Path::new(".");

	let search_paths = extract_paths(&matches, current_dir)?;
	let search_config = build_config(&matches)?;

	//scan::walk_dirs(&search_paths, &search_config);

	// TODO: Do the work on the duplicate files according to the set flags

	Ok(true)
}

fn extract_paths(matches: &clap::ArgMatches, current_dir: &Path) -> Result<Vec<PathBuf>, Error> {
	let search_paths = matches
		.values_of_os("path")
		.map_or_else(
			|| vec![current_dir.to_path_buf()],
			|paths| {
				paths.filter_map(|path| {
					let path_buffer = PathBuf::from(path);
					if filesystem::is_existing_directory(&path_buffer) {
						Some(path_buffer)
					} else {
						print_stderr(Error::new(format!(
							"Path '{}' is not a directory.",
							path_buffer.to_string_lossy()
						)));
						None
					}
				}).collect()
			},
		);

	if search_paths.is_empty() {
		return Err(Error::from("No valid paths given."));
	}

	Ok(search_paths)
}

fn build_config(matches: &clap::ArgMatches) -> Result<SearchConfig, Error> {

	let min = parse_config_size(matches.value_of("minsize"))?;
	let max = parse_config_size(matches.value_of("maxsize"))?;

	let search_config = SearchConfig{
		recursive:           matches.is_present("recursive"),
		ignore_empty_files:  matches.is_present("noempty"),
		ignore_hidden_files: matches.is_present("nohidden"),
		minsize: min.unwrap_or(0),
		maxsize: max.unwrap_or(u64::MAX)
	};

	Ok(search_config)
}