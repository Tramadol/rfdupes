use clap::{crate_version, App, AppSettings, Arg};

pub fn build_application() -> App<'static, 'static> {
	let clap_color_setting = if std::env::var_os("NO_COLOR").is_none() {
		AppSettings::ColoredHelp
	} else {
		AppSettings::ColorNever
	};

	let application = App::new("rfdupes")
		.version(crate_version!())
		.setting(clap_color_setting)
		.setting(AppSettings::DeriveDisplayOrder)
		.usage("rfdupes [FLAGS/OPTIONS] [<path>...]")
		.arg(
			Arg::with_name("path")
				.help("The directory/directories to search")
				.index(1)
				.multiple(true)
		)
		.arg(
			Arg::with_name("recursive")
				.long("recursive")
				.short("r")
				.help("Follow subdirectories in given paths")
		)
		.arg(
			Arg::with_name("delete")
				.long("delete")
				.short("d")
				.help("Deletes duplicate files")
		)
		.arg(
			Arg::with_name("noempty")
				.long("noempty")
				.short("E")
				.help("Exclude zero-length files from consideration")
		)
		.arg(
			Arg::with_name("nohidden")
				.long("nohidden")
				.short("H")
				.help("Exclude hidden files from consideration")
		)
		.arg(
			Arg::with_name("minsize")
				.long("min")
				.takes_value(true)
				.value_name("size")
				.number_of_values(1)
				.help("Minimum file size to consider")
		)
		.arg(
			Arg::with_name("maxsize")
				.long("max")
				.takes_value(true)
				.value_name("size")
				.number_of_values(1)
				.help("Maximum file size to consider")
		);

	application
}